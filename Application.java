public class Application
{
	public static void main(String[] args)
	{
		Student student1 = new Student();
		Student student2 = new Student();
		
		student1.studentID = 2233422;
		student2.studentID = 2234536;

		student1.email = "student1@dawsoncollege.qc.ca";
		student2.email = "student2@dawsoncollege.qc.ca";

		student1.password = "ST1_TM17";
		student2.password = "ST2_TW23";
		
		student1.sendEmail();
		student2.sendEmail();
		
		Student[] section3 = new Student[3];
		
		section3[0] = student1;
		section3[1] = student2;
		
		System.out.println(section3[0].email);
		// assign a new student OBJECT into that array position
		section3[2] = new Student();
		section3[2].email = "student3@dawsoncollege.qc.ca";
		section3[2].studentID = 2233546;
		section3[2].password = "TWM_2023";
		System.out.println(section3[2].password);
	}
}